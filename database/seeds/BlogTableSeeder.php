<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BlogTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('blog')->insert([
            'title' => '7 Makanan Khas Jawa',
            'content' => 'Jakarta - Makanan Tradisional',
            'category' => 'HUT RI ke-75'
        ]);
    }
}
